import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pizzashop10FilterComponent } from './pizzashop10-filter.component';

describe('Pizzashop10FilterComponent', () => {
  let component: Pizzashop10FilterComponent;
  let fixture: ComponentFixture<Pizzashop10FilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pizzashop10FilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pizzashop10FilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
