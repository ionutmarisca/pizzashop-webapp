import {Component, Input, OnInit} from '@angular/core';
import {PizzaService} from "app/shared/pizza.service";
import {Pizza} from "../shared/pizza";
import {Location} from '@angular/common';

@Component({
  selector: 'app-pizzashop5',
  templateUrl: './pizzashop10-new.component.html',
  styleUrls: ['./pizzashop10-new.component.css']
})
export class Pizzashop10NewComponent implements OnInit {
  @Input() pizza: Pizza;

  constructor(private pizzaService: PizzaService, private location: Location) { }

  ngOnInit() {
  }

  goBack(): void {
    this.location.back();
  }

  save(name, description, price, cuisine): void {
    console.log("*Adding new pizza* ", name, description, price, cuisine);
    this.pizzaService.create(name, description, price, cuisine)
      .subscribe(_ => _);
  }
}
