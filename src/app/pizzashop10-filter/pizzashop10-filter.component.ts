import { Component, OnInit } from '@angular/core';
import {PizzaService} from "../shared/pizza.service";
import {Pizza} from "../shared/pizza";
import {isUndefined} from "util";

@Component({
  selector: 'app-pizzashop10-filter',
  templateUrl: './pizzashop10-filter.component.html',
  styleUrls: ['./pizzashop10-filter.component.css']
})
export class Pizzashop10FilterComponent implements OnInit {
  priceLessThanCheckbox: boolean = false;
  cuisineCheckbox: boolean = false;
  pizzas: Pizza[];
  priceLessThanValue: number;
  cuisineValue: string;

  constructor(private pizzaService: PizzaService) { }

  ngOnInit() {
  }

  showAll(pizzaList) {

  }

  filter(): void {
    console.log("*Filter values* ", this.priceLessThanValue, this.cuisineValue);
    let allFilters = {filters: []};
    if(this.priceLessThanCheckbox) {
      console.log("First filter checked.");
      if(isUndefined(this.priceLessThanValue)) {
        alert("Less than filter doesn't provide any value.");
        return
      }
      allFilters.filters.push({
        filterType: "priceLessThan",
        filterValue: this.priceLessThanValue + ""
      });
    }
    if(this.cuisineCheckbox) {
      console.log("Second filter checked.");
      if (isUndefined(this.cuisineValue)) {
        alert("Cuisine value is not selected.");
        return
      }
      allFilters.filters.push({
        filterType: "cuisine",
        filterValue: this.cuisineValue + ""
      });
    }
    this.pizzaService.filter(allFilters).subscribe(pizzas => this.pizzas = pizzas);
  }

}
