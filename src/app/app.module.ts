import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { Pizzashop10NewComponent } from './pizzashop10-new/pizzashop10-new.component';
import {AppRoutingModule} from "./app-routing.module";
import {PizzaService} from "./shared/pizza.service";
import { Pizzashop10FilterComponent } from './pizzashop10-filter/pizzashop10-filter.component';

@NgModule({
  declarations: [
    AppComponent,
    Pizzashop10NewComponent,
    Pizzashop10FilterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [PizzaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
