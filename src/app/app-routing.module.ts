import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {Pizzashop10NewComponent} from "./pizzashop10-new/pizzashop10-new.component";
import {Pizzashop10FilterComponent} from "./pizzashop10-filter/pizzashop10-filter.component";

const routes: Routes = [
  { path: '', redirectTo: 'pizzashop10/new', pathMatch: 'full' },
  { path: 'pizzashop10/new',     component: Pizzashop10NewComponent },
  { path: 'pizzashop10/filter',     component: Pizzashop10FilterComponent }
];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
